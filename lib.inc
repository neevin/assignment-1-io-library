section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
        mov rax, 60
        syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length: 
        mov rax, 0
    .loop:
        cmp byte[rdi + rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string: 
        call string_length
        mov rsi, rdi
        mov rdx, rax
        mov rax, 1
        mov rdi, 1
        syscall ; вызов write
        ret


; Принимает код символа и выводит его в stdout
print_char: 
        push rdi ; Ложим символ на стек
        mov rsi, rsp ; Указываем на вершину стека, т.е. на символ, который нужно напечатать
        mov rdx, 1 ; Длина = 1
        mov rax, 1
        mov rdi, 1
        syscall
        pop rdi
        ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline: 
        mov rdi, 0xA
        call print_char
        ret


; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint: 
        mov rax, 0
        mov rcx, 0
        mov r9, 10
        dec rsp
        mov [rsp], al
        mov rax, rdi

    .loop:
        xor rdx, rdx
        div r9
        add dl, '0'
        dec rsp
        mov [rsp], dl
        inc rcx
        cmp rax, 0
        jne .loop

        mov rdi, rsp ; Печатаем строку
        push rcx
        call print_string
        pop rcx
        add rsp, rcx
        inc rsp
        ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int: 
        test rdi, rdi
        jns print_uint
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
    .print_uint:
        call print_uint
        ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rdi - указатель на начало 1 строки
; rsi - указатель на начало 2 строки
string_equals: 
        xor rdx, rdx ; Обнуляем счётчик

    .loop:
        cmp byte [rdi + rdx], 0
        je .first_end
        
        mov cl, byte [rdi + rdx]
        cmp cl, byte [rsi + rdx]
        jne .not_equals
        inc rdx
        jmp .loop

    .first_end: ; Первая строка закончилась
        cmp byte [rsi + rdx], 0
        je .equals ; Если вторая тоже закончилась
        jmp .not_equals

    .not_equals:
        mov rax, 0
        ret

    .equals:
        mov rax, 1
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
        xor rax, rax ; В rax ложим 0, номер системного вызова 'read'
        push rax
        xor rdi, rdi ; В rdi 0, номер файлового дескриптора stdin
        mov rsi, rsp ; В rsi адрес куда прочитать
        mov rdx, 1  ; В rdx размер буфер
        syscall ; execute read(0, buffer, BUFSIZE)
        pop rax
        ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; rdi - адрес начала буфера
; rsi - размер буфера
read_word: 
        mov r8, 0 ; Счётчик
        mov r9, 0 ; Длина числа
        mov rcx, 0 ; Флаг, был ли уже непробельный символ

    .read_one:
        push rdi
        push rsi
        push rcx
        call read_char
        pop rcx
        pop rsi
        pop rdi

        cmp al, 0x9 ; Табуляция
        je .space
        cmp al, 0xA ; Перевод строки
        je .space
        cmp al, 0x20 ; Пробел
        je .space
        mov rcx, 1 ; Строка началась

    .store:
        cmp rsi, r9 ; Есть ли ещё место в буффере
        jle .err
        mov [rdi + r9], rax ; Записываем очередной символ в буффер
        cmp rax, 0
        je .success
        inc r9
        jmp .read_one

    .space:
        cmp rcx, 0
        je .read_one
        xor rax, rax
        jmp .store

    .success:
        mov byte [rdi + r9], 0 ; Дописываем в конец терминирующий ноль
        mov rax, rdi
        mov rdx, r9
        ret

    .err:
        mov rax, 0
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint: 
        xor rax, rax
        xor rdx, rdx
        mov r9, 10
        lea rdi, [rdi]
    .loop:
        cmp byte [rdi], '0'
        jb .end
        cmp byte [rdi], '9'
        ja .end
        push rdx
        mul r9 ; Результат записаывается в (rdx rax)
        pop rdx
        add al, byte [rdi]
        sub al, '0'
        inc rdx
        inc rdi
        lea rdi, [rdi]
        jmp .loop
    .end:
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int: 
        cmp byte [rdi], '-'
        je .negative
        call parse_uint
        ret 
	.negative:
		inc rdi
		call parse_uint
		neg rax
		inc rdx ; Увеличиваем на 1, потому что впереди минус
		ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi - указатель на начало строки
; rsi - указатель на буфер
; rdx - длина буфера
string_copy: 
        call string_length ; rax = len(str)
        cmp rdx, rax ; rdx - rax
        jbe .end
        mov rdx, rax
    .loop:
        mov rcx, [rdi]
        mov [rsi], rcx
        inc rsi
        inc rdi
        dec rax
        jne .loop
        mov byte[rsi], 0
        mov rax, rdx
        ret
    .end:
        mov rax, 0
        ret
